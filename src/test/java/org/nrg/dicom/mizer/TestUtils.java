package org.nrg.dicom.mizer;

import org.nrg.dicom.mizer.objects.DicomObjectI;

public class TestUtils {
    public static class TestTag {
        public int tag;
        public String initialValue, postValue;
        public TestTag( int tag, String initialValue, String postValue) {
            this.tag = tag;
            this.initialValue = initialValue;
            this.postValue = postValue;
        }
        public TestTag( int tag, String value) {
            this( tag, value, value);
        }
        public String valueFrom( DicomObjectI dobj) {
            return dobj.getString( tag);
        }
    }
    public static class TestSeqTag {
        public int[] tag;
        public String initialValue, postValue;
        public TestSeqTag( int[] tag, String initialValue, String postValue) {
            this.tag = tag;
            this.initialValue = initialValue;
            this.postValue = postValue;
        }
        public TestSeqTag( int[] tag, String value) {
            this(tag, value, value);
        }
        public String valueFrom( DicomObjectI dobj) {
            return dobj.getString( tag);
        }
    }

    public static void put( DicomObjectI dobj, TestTag tag) {
        dobj.putString( tag.tag, tag.initialValue);
    }

    public static void put( DicomObjectI dobj, TestSeqTag tag) {
        dobj.putString( tag.tag, tag.initialValue);
    }

}
