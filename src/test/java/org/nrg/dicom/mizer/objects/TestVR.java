package org.nrg.dicom.mizer.objects;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.nrg.dicom.mizer.TestUtils.put;
import static org.nrg.dicom.mizer.TestUtils.TestTag;

/**
 * These are the known VRs as of 2023d
 * Application Entity AE
 * Age String AS
 * Attribute Tag AT
 * Code String CS
 * Date  DA
 * Decimal String DS
 * DateTime DT
 * Floating Point Single FL
 * Floating Point Double FD
 * Integer String IS
 * Long String LO
 * Long Text LT
 * Other Byte OB
 * Other Double OD
 * Other Float OF
 * Other Long OL
 * Other Very Long OV
 * Other Word OW
 * Person Name PN
 * Short String SH
 * Signed Long SL
 * Sequence Items SQ
 * Signed Short SS
 * Short Text ST
 * Signed 64-bit very Long SV
 * Time TM
 * Unlimited Characters UC
 * Unique Identifier UI
 * Unsigned Long UL
 * Unknown UN
 * URL or URI UR
 * Unsigned Short US
 * Unlimited Text UT
 * Unsigned 64-bit Very Long UV
 *
 *  The file dicom/vr.dcm has random tags representing most VRs. currently missing are
 *  at, od, ol, ov, ow, sl, ss, sv, ut,
 * (0002,0000) UL #4 [44] FileMetaInformationGroupLength
 * (0002,0002) UI #0 [] MediaStorageSOPClassUID
 * (0002,0003) UI #0 [] MediaStorageSOPInstanceUID
 * (0002,0010) UI #20 [1.2.840.10008.1.2.1] TransferSyntaxUID
 * (0008,0005) CS #12 [codedString] SpecificCharacterSet
 * (0008,0013) TM #6 [132433] InstanceCreationTime
 * (0008,0020) DA #8 [19600519] StudyDate
 * (0008,0054) AE #8 [aeTitle] RetrieveAETitle
 * (0008,0119) UC #20 [unlimitedCharacters] LongCodeValue
 * (0008,040D) UV #8 [19600519] FileLengthInContainer
 * (0008,0414) US #2 [42] RequestedStatusInterval
 * (0008,0418) LT #8 [longText] TransactionStatusComment
 * (0008,041B) OB #6 [-94\-40\94\-84\28\-83] RecordKey
 * (0008,0427) UL #4 [22222] NumberOfStudyRecordsInInstance
 * (0008,1160) IS #6 [12345] ReferencedFrameNumber
 * (0008,1163) FD #8 [6.28] TimeRange
 * (0008,1190) UR #18 [https://org.snafu] RetrieveURL
 * (0008,2130) DS #4 [-42] EventElapsedTimes
 * (0008,2132) LO #10 [longString] EventTimerNames
 * (0008,9459) FL #4 [3.14] RecommendedDisplayFrameRateInFloat
 * (0010,0010) PN #8 [Doe^John] PatientName
 * (0010,1010) AS #4 [063Y] PatientAge
 * (0010,2160) SH #12 [shortString] EthnicGroup
 * (0012,0051) ST #10 [shortText] ClinicalTrialTimePointDescription
 * (0018,1638) OF #4 [1.2] VerticesOfThePolygonalOutline
 * (0018,991E) UI #10 [1.2.3.4.5] TargetFrameOfReferenceUID
 * (6666,6666) UN #4 [-70\121\39\0]
 */
public class TestVR {

    /**
     * Replace values in existing tags. The VR should not change.
     *
     * @throws Exception
     */
    @Test
    public void testDontChangeExistingVR() throws Exception {
        ClassLoader loader = TestVR.class.getClassLoader();
        File file = new File(loader.getResource("dicom/vr.dcm").toURI());
        DicomObjectI dobj = DicomObjectFactory.newInstance(file);

        // Application Entity AE
        TestTag aeTag = new TestTag( 0x00080054, "aeTitle", "");
        // Age String AS
        TestTag asTag = new TestTag( 0x00101010, "063Y", "");
        // Attribute Tag AT
//        TestTag atTag = new TestTag( 0x0000000, "19600519", "");
        // Code String CS
        TestTag csTag = new TestTag( 0x00080005, "codedString", "");
        // Date  DA
        TestTag daTag = new TestTag( 0x00080020, "19600519", "");
        // Decimal String DS
        TestTag dsTag = new TestTag( 0x00082130, "-42", "");
        // DateTime DT
        TestTag dtTag = new TestTag( 0x0008002A, "19600519132115", "");
        // Floating Point Single FL
        TestTag flTag = new TestTag( 0x00089459, "3.14", "");
        // Floating Point Double FD
        TestTag fdTag = new TestTag( 0x00081163, "6.28", "");
        // Integer String IS
        TestTag isTag = new TestTag( 0x00081160, "12345", "");
        // Long String LO
        TestTag loTag = new TestTag( 0x00082132, "longString", "");
        // Long Text LT
        TestTag ltTag = new TestTag( 0x00080418, "longText", "");
        // Other Byte OB
        TestTag obTag = new TestTag( 0x0008041B, "otherByte", "");
        // Other Double OD
        TestTag odTag = new TestTag( 0x003A032E, "19600519", "");
        // Other Float OF
        TestTag ofTag = new TestTag( 0x00181638, "1.2", "");
        // Other Long OL
//        TestTag olTag = new TestTag( 0x0000000, "19600519", "");
        // Other Very Long OV
//        TestTag ovTag = new TestTag( 0x0000000, "19600519", "");
        // Other Word OW
//        TestTag owTag = new TestTag( 0x0000000, "19600519", "");
        // Person Name PN
        TestTag pnTag = new TestTag( 0x00100010, "Doe^John", "");
        // Short String SH
        TestTag shTag = new TestTag( 0x00102160, "shortString", "");
        // Signed Long SL
//        TestTag slTag = new TestTag( 0x0000000, "1111", "");
        // Sequence Items SQ
        TestTag sqTag = new TestTag( 0x00081198, "", "");
        // Signed Short SS
//        TestTag ssTag = new TestTag( 0x0000000, "19600519", "");
        // Short Text ST
        TestTag stTag = new TestTag( 0x00120051, "shortText", "");
        // Signed 64-bit very Long SV
//        TestTag svTag = new TestTag( 0x0000000, "19600519", "");
        // Time TM
        TestTag tmTag = new TestTag( 0x00080013, "132433", "");
        // Unlimited Characters UC
        TestTag ucTag = new TestTag( 0x00080119, "unlimitedCharacters", "");
        // Unique Identifier UI
        TestTag uiTag = new TestTag( 0x0018991E, "1.2.3.4.5", "");
        // Unsigned Long UL
        TestTag ulTag = new TestTag( 0x00080427, "22222", "");
        // Unknown UN
        TestTag unTag = new TestTag( 0x66666666, "unknown", "");
        // URL or URI UR
        TestTag urTag = new TestTag( 0x00081190, "https://org.snafu", "");
        // Unsigned Short US
        TestTag usTag = new TestTag( 0x00080414, "42", "");
        // Unlimited Text UT
//        TestTag utTag = new TestTag( 0x0000000, "unlimitedText", "");
        // Unsigned 64-bit Very Long UV
        TestTag uvTag = new TestTag( 0x0008040D, "19600519", "");

        dobj.putString( aeTag.tag, aeTag.postValue);
        dobj.putString( asTag.tag, asTag.postValue);
        dobj.putString( csTag.tag, csTag.postValue);
        dobj.putString( daTag.tag, daTag.postValue);
        dobj.putString( dsTag.tag, dsTag.postValue);
        dobj.putString( fdTag.tag, fdTag.postValue);
        dobj.putString( flTag.tag, flTag.postValue);
        dobj.putString( isTag.tag, isTag.postValue);
        dobj.putString( loTag.tag, loTag.postValue);
        dobj.putString( ltTag.tag, ltTag.postValue);
        dobj.putString( obTag.tag, obTag.postValue);
        dobj.putString( odTag.tag, odTag.postValue);
        dobj.putString( ofTag.tag, ofTag.postValue);
        dobj.putString( pnTag.tag, pnTag.postValue);
        dobj.putString( shTag.tag, shTag.postValue);
//        dobj.putString( sqTag.tag, sqTag.postValue);
        dobj.putString( stTag.tag, stTag.postValue);
        dobj.putString( tmTag.tag, tmTag.postValue);
//        dobj.putString( ucTag.tag, ucTag.postValue);
        dobj.putString( uiTag.tag, uiTag.postValue);
        dobj.putString( ulTag.tag, ulTag.postValue);
        dobj.putString( unTag.tag, unTag.postValue);
        dobj.putString( urTag.tag, urTag.postValue);
        dobj.putString( usTag.tag, usTag.postValue);
        dobj.putString( uvTag.tag, uvTag.postValue);

        assertEquals( "AE", dobj.get(aeTag.tag).getVRAsString());
        assertEquals( "AS", dobj.get(asTag.tag).getVRAsString());
//        assertEquals( "AT", dobj.get(atTag.tag).getVRAsString());
        assertEquals( "CS", dobj.get(csTag.tag).getVRAsString());
        assertEquals( "DA", dobj.get(daTag.tag).getVRAsString());
        assertEquals( "DS", dobj.get(dsTag.tag).getVRAsString());
//        assertEquals( "DT", dobj.get(dtTag.tag).getVRAsString());
        assertEquals( "FD", dobj.get(fdTag.tag).getVRAsString());
        assertEquals( "FL", dobj.get(flTag.tag).getVRAsString());
        assertEquals( "IS", dobj.get(isTag.tag).getVRAsString());
        assertEquals( "LO", dobj.get(loTag.tag).getVRAsString());
        assertEquals( "LT", dobj.get(ltTag.tag).getVRAsString());
        assertEquals( "OB", dobj.get(obTag.tag).getVRAsString());
//        assertEquals( "OD", dobj.get(odTag.tag).getVRAsString());
        assertEquals( "OF", dobj.get(ofTag.tag).getVRAsString());
//        assertEquals( "OL", dobj.get(olTag.tag).getVRAsString());
//        assertEquals( "OV", dobj.get(ovTag.tag).getVRAsString());
//        assertEquals( "OW", dobj.get(owTag.tag).getVRAsString());
        assertEquals( "PN", dobj.get(pnTag.tag).getVRAsString());
        assertEquals( "SH", dobj.get(shTag.tag).getVRAsString());
//        assertEquals( "SL", dobj.get(slTag.tag).getVRAsString());
//        assertEquals( "SS", dobj.get(ssTag.tag).getVRAsString());
//        assertEquals( "SQ", dobj.get(sqTag.tag).getVRAsString());
        assertEquals( "ST", dobj.get(stTag.tag).getVRAsString());
//        assertEquals( "SV", dobj.get(svTag.tag).getVRAsString());
        assertEquals( "TM", dobj.get(tmTag.tag).getVRAsString());
//        assertEquals( "UC", dobj.get(ucTag.tag).getVRAsString());
        assertEquals( "UI", dobj.get(uiTag.tag).getVRAsString());
        assertEquals( "UL", dobj.get(ulTag.tag).getVRAsString());
        assertEquals( "UN", dobj.get(unTag.tag).getVRAsString());
        assertEquals( "UN", dobj.get(urTag.tag).getVRAsString());  // dcm4che 2 does not know UR
        assertEquals( "US", dobj.get(usTag.tag).getVRAsString());
//        assertEquals( "UT", dobj.get(utTag.tag).getVRAsString());
        assertEquals( "UN", dobj.get(uvTag.tag).getVRAsString());  // dcm4che 2 does not know UV
    }

    @Test
    public void testVRofNewTags() throws Exception {
        DicomObjectI dobj = DicomObjectFactory.newInstance();

        // Application Entity AE
        TestTag aeTag = new TestTag( 0x00080054, "aeTitle", "");
        // Age String AS
        TestTag asTag = new TestTag( 0x00101010, "063Y", "");
        // Attribute Tag AT
        TestTag atTag = new TestTag( 0x00205000, "19600519", "");
        // Code String CS
        TestTag csTag = new TestTag( 0x00080005, "codedString", "");
        // Date  DA
        TestTag daTag = new TestTag( 0x00080020, "19600519", "");
        // Decimal String DS
        TestTag dsTag = new TestTag( 0x00082130, "-42", "");
        // DateTime DT
        TestTag dtTag = new TestTag( 0x0008002A, "19600519132115", "");
        // Floating Point Single FL
        TestTag flTag = new TestTag( 0x00089459, "3.14", "");
        // Floating Point Double FD
        TestTag fdTag = new TestTag( 0x00081163, "6.28", "");
        // Integer String IS
        TestTag isTag = new TestTag( 0x00081160, "12345", "");
        // Long String LO
        TestTag loTag = new TestTag( 0x00082132, "longString", "");
        // Long Text LT
        TestTag ltTag = new TestTag( 0x00324000, "longText", "");
        // Other Byte OB
        TestTag obTag = new TestTag( 0x00686300, "otherByte", "");
        // Other Double OD
        // not finding an OD in dcm4che-2 dictionary.
//        TestTag odTag = new TestTag( 0x7FE00009, "1960.0519", "");
        // Other Float OF
        TestTag ofTag = new TestTag( 0x00640009, "1.2", "");
        // Other Long OL
        // not finding an OL in dcm4che-2 dictionary.
//        TestTag olTag = new TestTag( 0x00720075, "19600519", "");
        // Other Very Long OV
        // not finding an OV in dcm4che-2 dictionary.
//        TestTag ovTag = new TestTag( 0x7FE00001, "19600519", "");
        // Other Word OW
        TestTag owTag = new TestTag( 0x00281201, "19600519", "");
        // Person Name PN
        TestTag pnTag = new TestTag( 0x00100010, "Doe^John", "");
        // Short String SH
        TestTag shTag = new TestTag( 0x00102160, "shortString", "");
        // Signed Long SL
        TestTag slTag = new TestTag( 0x00186020, "1111", "");
        // Sequence Items SQ
        TestTag sqTag = new TestTag( 0x00081198, "", "");
        // Signed Short SS
        TestTag ssTag = new TestTag( 0x00189219, "19600519", "");
        // Short Text ST
        TestTag stTag = new TestTag( 0x00120051, "shortText", "");
        // Signed 64-bit very Long SV
        // not finding an SV in dcm4che-2 dictionary.
//        TestTag svTag = new TestTag( 0x00720082, "19600519", "");
        // Time TM
        TestTag tmTag = new TestTag( 0x00080013, "132433", "");
        // Unlimited Characters UC
        // not finding an UC in dcm4che-2 dictionary.
//        TestTag ucTag = new TestTag( 0x3010001B, "unlimitedCharacters", "");
        // Unique Identifier UI
        TestTag uiTag = new TestTag( 0x00080014, "1.2.3.4.5", "");
        // Unsigned Long UL
        TestTag ulTag = new TestTag( 0x00081161, "22222", "");
        // Unknown UN
        TestTag unTag = new TestTag( 0x66666666, "unknown", "");
        // URL or URI UR
        TestTag urTag = new TestTag( 0x00081190, "https://org.snafu", "");
        // Unsigned Short US
        TestTag usTag = new TestTag( 0x00081197, "42", "");
        // Unlimited Text UT
        // not finding an UT in dcm4che-2 dictionary.
//        TestTag utTag = new TestTag( 0x00321066, "unlimitedText", "");
        // Unsigned 64-bit Very Long UV
        TestTag uvTag = new TestTag( 0x0008040D, "19600519", "");

        put( dobj, aeTag);
        put( dobj, asTag);
        put( dobj, atTag);
        put( dobj, csTag);
        put( dobj, daTag);
        put( dobj, dsTag);
        put( dobj, dtTag);
        put( dobj, fdTag);
        put( dobj, flTag);
        put( dobj, isTag);
        put( dobj, loTag);
        put( dobj, ltTag);
        put( dobj, obTag);
//        put( dobj, odTag);
        put( dobj, ofTag);
//        put( dobj, olTag);
//        put( dobj, ovTag);
        put( dobj, owTag);
        put( dobj, pnTag);
        put( dobj, shTag);
        put( dobj, slTag);
//        put( dobj, sqTag);
        put( dobj, ssTag);
        put( dobj, stTag);
//        put( dobj, svTag);
        put( dobj, tmTag);
//        put( dobj, ucTag);
        put( dobj, uiTag);
        put( dobj, ulTag);
        put( dobj, unTag);
        put( dobj, urTag);
        put( dobj, usTag);
//        put( dobj, utTag);
        put( dobj, uvTag);

        assertEquals( "AE", dobj.get(aeTag.tag).getVRAsString());
        assertEquals( "AS", dobj.get(asTag.tag).getVRAsString());
        assertEquals( "AT", dobj.get(atTag.tag).getVRAsString());
        assertEquals( "CS", dobj.get(csTag.tag).getVRAsString());
        assertEquals( "DA", dobj.get(daTag.tag).getVRAsString());
        assertEquals( "DS", dobj.get(dsTag.tag).getVRAsString());
        assertEquals( "DT", dobj.get(dtTag.tag).getVRAsString());
        assertEquals( "FD", dobj.get(fdTag.tag).getVRAsString());
        assertEquals( "FL", dobj.get(flTag.tag).getVRAsString());
        assertEquals( "IS", dobj.get(isTag.tag).getVRAsString());
        assertEquals( "LO", dobj.get(loTag.tag).getVRAsString());
        assertEquals( "LT", dobj.get(ltTag.tag).getVRAsString());
        assertEquals( "OB", dobj.get(obTag.tag).getVRAsString());
//        assertEquals( "OD", dobj.get(odTag.tag).getVRAsString());
        assertEquals( "OF", dobj.get(ofTag.tag).getVRAsString());
//        assertEquals( "OL", dobj.get(olTag.tag).getVRAsString());
//        assertEquals( "OV", dobj.get(ovTag.tag).getVRAsString());
        assertEquals( "OW", dobj.get(owTag.tag).getVRAsString());
        assertEquals( "PN", dobj.get(pnTag.tag).getVRAsString());
        assertEquals( "SH", dobj.get(shTag.tag).getVRAsString());
        assertEquals( "SL", dobj.get(slTag.tag).getVRAsString());
        assertEquals( "SS", dobj.get(ssTag.tag).getVRAsString());
//        assertEquals( "SQ", dobj.get(sqTag.tag).getVRAsString());
        assertEquals( "ST", dobj.get(stTag.tag).getVRAsString());
//        assertEquals( "SV", dobj.get(svTag.tag).getVRAsString());
        assertEquals( "TM", dobj.get(tmTag.tag).getVRAsString());
//        assertEquals( "UC", dobj.get(ucTag.tag).getVRAsString());
        assertEquals( "UI", dobj.get(uiTag.tag).getVRAsString());
        assertEquals( "UL", dobj.get(ulTag.tag).getVRAsString());
        assertEquals( "UN", dobj.get(unTag.tag).getVRAsString());
        assertEquals( "UN", dobj.get(urTag.tag).getVRAsString());  // dcm4che 2 does not know UR
        assertEquals( "US", dobj.get(usTag.tag).getVRAsString());
//        assertEquals( "UT", dobj.get(utTag.tag).getVRAsString());
        assertEquals( "UN", dobj.get(uvTag.tag).getVRAsString());  // dcm4che 2 does not know UV
    }
}
